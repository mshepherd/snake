package sondow.snake.game

import spock.lang.Specification

class TargetSpec extends Specification {

    def "should be adjacent to nearby points only"() {
        setup:
        Point snakeHead = new Point(sRow, sCol)
        Target target = new Target(tRow, tCol)

        when:
        boolean result = target.isAdjacentTo(snakeHead)

        then:
        result == isAdjacent

        where:
        isAdjacent | sRow | sCol | tRow | tCol
        false      | 4    | 6    | 0    | 6
        true       | 4    | 6    | 2    | 6
        false      | 4    | 6    | 4    | 6
        true       | 4    | 6    | 6    | 6
        false      | 4    | 6    | 8    | 6
        false      | 4    | 6    | 10   | 6

        false      | 4    | 6    | 4    | 0
        false      | 4    | 6    | 4    | 2
        true       | 4    | 6    | 4    | 4
        false      | 4    | 6    | 4    | 6
        true       | 4    | 6    | 4    | 8
        false      | 4    | 6    | 4    | 10

        false      | 4    | 0    | 8    | 2
        false      | 4    | 2    | 8    | 2
        false      | 4    | 4    | 8    | 2
        false      | 4    | 6    | 8    | 2
        false      | 4    | 8    | 8    | 2
        false      | 4    | 10   | 8    | 2

        false      | 6    | 0    | 8    | 2
        true       | 6    | 2    | 8    | 2
        false      | 6    | 4    | 8    | 2
        false      | 6    | 6    | 8    | 2
        false      | 6    | 8    | 8    | 2
        false      | 6    | 10   | 8    | 2
    }
}
