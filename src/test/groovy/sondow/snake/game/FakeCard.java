package sondow.snake.game;

import java.time.Instant;
import twitter4j.Card;
import twitter4j.Choice;

public class FakeCard implements Card {

    private Choice[] choices;

    private boolean countsAreFinal;

    /**
     * @return the name
     */
    @Override
    public String getName() {
        return null;
    }

    /**
     * @return the cardUrl
     */
    @Override
    public String getCardUrl() {
        return null;
    }

    /**
     * @return the api
     */
    @Override
    public String getApi() {
        return null;
    }

    /**
     * @return the durationMinutes
     */
    @Override
    public int getDurationMinutes() {
        return 0;
    }

    /**
     * @return the countsAreFinal
     */
    @Override
    public boolean isCountsAreFinal() {
        return countsAreFinal;
    }

    public FakeCard setCountsAreFinal(boolean countsAreFinal) {
        this.countsAreFinal = countsAreFinal;
        return this;
    }

    /**
     * @return the lastUpdatedDateTimeUtc
     */
    @Override
    public Instant getLastUpdatedDateTimeUtc() {
        return null;
    }

    /**
     * @return the endDatetimeUtc
     */
    @Override
    public Instant getEndDatetimeUtc() {
        return null;
    }

    /**
     * @return the poll choices
     */
    @Override
    public Choice[] getChoices() {
        return choices;
    }

    public FakeCard setChoices(Choice[] choices) {
        this.choices = choices;
        return this;
    }

    /**
     * @return the unique identifier of the card
     */
    @Override
    public String getUrl() {
        return null;
    }
}
