package sondow.snake.io

import com.sybit.airtable.Airtable
import com.sybit.airtable.Base
import com.sybit.airtable.Query
import com.sybit.airtable.Table
import sondow.snake.conf.GameConfig
import spock.lang.Specification
import spock.lang.Unroll
import twitter4j.conf.Configuration

class AirtableDatabaseSpec extends Specification {

    @Unroll("on db read-write if query result is #queryResult then creation count is #creationCount")
    def 'should read state from database and create record if necessary, then write new state'() {

        setup:
        Airtable airtable = Mock()
        GameConfig config = Mock()
        Base base = Mock()
        Table table = Mock()
        Configuration twitterConfig = Mock()
        Query query = new AirtableBotStateQuery('SchoolsOfFish')
        Database database = new AirtableDatabase(airtable, config)

        when:
        String gameState = database.readGameState()
        database.writeGameState('thenewgamestate')

        then:
        1 * config.getAirtableBaseId() >> '456abc'
        1 * airtable.base('456abc') >> base
        1 * base.table('SNAKE', AirtableRecord.class) >> table
        1 * config.getTwitterConfig() >> twitterConfig
        1 * twitterConfig.getUser() >> 'SchoolsOfFish'
        1 * table.select(query) >> queryResult
        creationCount * table.create(new AirtableRecord(name: 'SchoolsOfFish'))
        creationCount * table.select(query) >> [new AirtableRecord(name: 'SchoolsOfFish')]
        1 * table.update(new AirtableRecord(name: updatedName, state: 'thenewgamestate'))
        0 * _._
        gameState == expected

        where:
        queryResult                            | creationCount | expected  | updatedName
        [new AirtableRecord(state: 'E2J3d31')] | 0             | "E2J3d31" | null
        [new AirtableRecord(state: '')]        | 0             | null      | null
        [new AirtableRecord(state: null)]      | 0             | null      | null
        []                                     | 1             | null      | 'SchoolsOfFish'
    }

    @Unroll("on db write if query result is #queryResult then creation count is #creationCount")
    def 'should write state to db even if record is not cached and create record if necessary'() {

        setup:
        Airtable airtable = Mock()
        GameConfig config = Mock()
        Base base = Mock()
        Table table = Mock()
        Configuration twitterConfig = Mock()
        Query query = new AirtableBotStateQuery('SchoolsOfFish')
        Database database = new AirtableDatabase(airtable, config)

        when:
        database.writeGameState('santa')

        then:
        1 * config.getAirtableBaseId() >> '456abc'
        1 * airtable.base('456abc') >> base
        1 * base.table('SNAKE', AirtableRecord.class) >> table
        1 * config.getTwitterConfig() >> twitterConfig
        1 * twitterConfig.getUser() >> 'SchoolsOfFish'
        1 * table.select(query) >> queryResult
        creationCount * table.create(new AirtableRecord(name: 'SchoolsOfFish'))
        creationCount * table.select(query) >> [new AirtableRecord(state: 'santa')]
        1 * table.update(new AirtableRecord(state: 'santa'))
        0 * _._

        where:
        queryResult                            | creationCount
        [new AirtableRecord(state: 'E2J3d31')] | 0
        [new AirtableRecord(state: '')]        | 0
        [new AirtableRecord(state: null)]      | 0
        []                                     | 1
    }
}
