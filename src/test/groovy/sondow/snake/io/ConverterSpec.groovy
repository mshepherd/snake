package sondow.snake.io

import sondow.snake.game.Game
import sondow.snake.game.Snake
import sondow.snake.game.Target
import spock.lang.Specification

class ConverterSpec extends Specification {

    def 'should convert database string v2 with atomicMovesSinceEating to game'() {

        setup:
        String contents = '{"thm":"TEST","thr":31,"sn":[0,6,"L2D4L4U4R2D2",7],"tg":[4,8],' +
                '"sc":80,"id":1234567890}'
        Converter converter = new Converter(new Random(4))
        String expected = "" +
                "Score 80\n" +
                "\n" +
                "🔵🔵🔵⭕🔵🔵😐⭕⭕\n" +
                "🔵⬛🔵⬛🔵⬛⭕⬛⭕\n" +
                "🔵⭕🔵⭕🔵⭕⭕⭕⭕\n" +
                "🔵⬛⭕⬛🔵⬛⭕🕔⭕\n" +
                "🔵🔵🔵🔵🔵⭕⭕⭕🔺\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕"
        Snake expectedSnake = new Snake().addKeyPoint(0, 6).addKeyPoint(0, 4).addKeyPoint(4, 4).
                addKeyPoint(4, 0).addKeyPoint(0, 0).addKeyPoint(0, 2).addKeyPoint(2, 2).
                setAtomicMovesSinceEating(7)
        Target expectedTarget = new Target(4, 8)

        when:
        Game game = converter.makeGameFromJson(contents)

        then:
        game.threadLength == 31
        game.toString() == expected
        game.score == 80
        game.snake == expectedSnake
        game.target == expectedTarget
        game.tweetId == 1234567890L
    }

    def 'should convert database string v1 without atomicMovesSinceEating to game'() {

        setup:
        String contents = '{"thm":"TEST","thr":31,"sn":[0,6,"L2D4L4U4R2D2"],"tg":[4,8],' +
                '"sc":80,"id":1234567890}'
        Converter converter = new Converter(new Random(4))
        String expected = "" +
                "Score 80\n" +
                "\n" +
                "🔵🔵🔵⭕🔵🔵😋⭕⭕\n" +
                "🔵⬛🔵⬛🔵⬛⭕⬛⭕\n" +
                "🔵⭕🔵⭕🔵⭕⭕⭕⭕\n" +
                "🔵⬛⭕⬛🔵⬛⭕🕛⭕\n" +
                "🔵🔵🔵🔵🔵⭕⭕⭕🔺\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕"
        Snake expectedSnake = new Snake().addKeyPoint(0, 6).addKeyPoint(0, 4).addKeyPoint(4, 4).
                addKeyPoint(4, 0).addKeyPoint(0, 0).addKeyPoint(0, 2).addKeyPoint(2, 2).
                setAtomicMovesSinceEating(0)
        Target expectedTarget = new Target(4, 8)

        when:
        Game game = converter.makeGameFromJson(contents)

        then:
        game.threadLength == 31
        game.toString() == expected
        game.score == 80
        game.snake == expectedSnake
        game.target == expectedTarget
        game.tweetId == 1234567890L
    }

    def 'should convert database string with a double-digit number to game'() {

        setup:
        String contents = ' {"sc":90,"tg":[10,0],"thm":"TEST","sn":[10,6,"R2U10L4D4",1],' +
                '"id":1098947596419133441,"thr":34}'
        Converter converter = new Converter(new Random(4))
        String expected = "" +
                "Score 90\n" +
                "\n" +
                "⭕⭕⭕⭕🔵🔵🔵🔵🔵\n" +
                "⭕⬛⭕⬛🔵⬛⭕⬛🔵\n" +
                "⭕⭕⭕⭕🔵⭕⭕⭕🔵\n" +
                "⭕⬛⭕⬛🔵⬛⭕⬛🔵\n" +
                "⭕⭕⭕⭕🔵⭕⭕⭕🔵\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛🔵\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕🔵\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛🔵\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕🔵\n" +
                "⭕🕚⭕⬛⭕⬛⭕⬛🔵\n" +
                "🔺⭕⭕⭕⭕⭕🙂🔵🔵"
        Snake expectedSnake = new Snake().addKeyPoint(10, 6).addKeyPoint(10, 8).
                addKeyPoint(0, 8).addKeyPoint(0, 4).addKeyPoint(4, 4).
                setAtomicMovesSinceEating(1)
        Target expectedTarget = new Target(10, 0)

        when:
        Game game = converter.makeGameFromJson(contents)

        then:
        game.threadLength == 34
        game.toString() == expected
        game.score == 90
        game.snake == expectedSnake
        game.target == expectedTarget
        game.tweetId == 1098947596419133441L
    }

    def 'should convert database string with many double-digit numbers to game'() {

        setup:
        String contents = ' {"sc":90,"tg":[10,0],"thm":"TEST",' +
                '"sn":[10,8,"U10L2D10L2U10L2D10",902],"id":1098947596419133441,"thr":34}'
        Converter converter = new Converter(new Random(4))
        String expected = "" +
                "Score 90\n" +
                "\n" +
                "⭕⭕🔵🔵🔵⭕🔵🔵🔵\n" +
                "⭕⬛🔵⬛🔵⬛🔵⬛🔵\n" +
                "⭕⭕🔵⭕🔵⭕🔵⭕🔵\n" +
                "⭕⬛🔵⬛🔵⬛🔵⬛🔵\n" +
                "⭕⭕🔵⭕🔵⭕🔵⭕🔵\n" +
                "⭕⬛🔵⬛🔵⬛🔵⬛🔵\n" +
                "⭕⭕🔵⭕🔵⭕🔵⭕🔵\n" +
                "⭕⬛🔵⬛🔵⬛🔵⬛🔵\n" +
                "⭕⭕🔵⭕🔵⭕🔵⭕🔵\n" +
                "⭕🚫🔵⬛🔵⬛🔵⬛🔵\n" +
                "🔺⭕🔵⭕🔵🔵🔵⭕😟"
        Snake expectedSnake = new Snake().addKeyPoint(10, 8).addKeyPoint(0, 8).
                addKeyPoint(0, 6).addKeyPoint(10, 6).addKeyPoint(10, 4).
                addKeyPoint(0, 4).addKeyPoint(0, 2).addKeyPoint(10, 2).
                setAtomicMovesSinceEating(902)
        Target expectedTarget = new Target(10, 0)

        when:
        Game game = converter.makeGameFromJson(contents)

        then:
        game.threadLength == 34
        game.toString() == expected
        game.score == 90
        game.snake == expectedSnake
        game.target == expectedTarget
        game.tweetId == 1098947596419133441L
    }

    def 'should roundtrip convert game to database string and then back into game etc'() {

        setup:
        Random random = new Random(4)
        Converter converter = new Converter(random)
        Game game = new Game(EmojiSet.TEST, random)
        Snake snake = new Snake().addKeyPoint(0, 6).addKeyPoint(0, 4).addKeyPoint(4, 4).
                addKeyPoint(4, 0).addKeyPoint(0, 0).addKeyPoint(0, 2).addKeyPoint(2, 2).
                setAtomicMovesSinceEating(15)
        Target target = new Target(4, 8)
        game.setSnake(snake).setTarget(target).setScore(200).setThreadLength(15).
                setTweetId(1234567890L)

        when:
        String databaseFormat = converter.makeJsonFromGame(game)
        Game newGame = converter.makeGameFromJson(databaseFormat)
        String newDatabaseFormat = converter.makeJsonFromGame(newGame)

        then:
        databaseFormat == '{"sc":200,"tg":[4,8],"thm":"TEST",' +
                '"sn":[0,6,"L2D4L4U4R2D2",15],"id":1234567890,"thr":15}'
        game == newGame
        databaseFormat == newDatabaseFormat
        game.toString() == "" +
                "Score 200\n" +
                "\n" +
                "🔵🔵🔵⭕🔵🔵😟⭕⭕\n" +
                "🔵⬛🔵⬛🔵⬛⭕⬛⭕\n" +
                "🔵⭕🔵⭕🔵⭕⭕⭕⭕\n" +
                "🔵⬛⭕⬛🔵⬛⭕🚫⭕\n" +
                "🔵🔵🔵🔵🔵⭕⭕⭕🔺\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕"
    }
}
