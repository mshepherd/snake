package sondow.snake.io

import spock.lang.Specification

class EmojiSetSpec extends Specification {

    def "should randomly pick each theme roughly equally"() {
        setup:
        List<EmojiSet> sets = new ArrayList<>()
        int totalRuns = 100000

        when:
        for (int i = 0; i < totalRuns; i++) {
            Random random = new Random(i)
            EmojiSet result = EmojiSet.pickOne(random)
            sets.add(result)
        }
        int commonEmojiSetCount = EmojiSet.common().size()
        BigDecimal portionSize = totalRuns / commonEmojiSetCount

        int testCount = Collections.frequency(sets, EmojiSet.TEST)

        int blueCount = Collections.frequency(sets, EmojiSet.BLUE)
        int breadCount = Collections.frequency(sets, EmojiSet.BREAD)
        int donutCount = Collections.frequency(sets, EmojiSet.DONUT)
        int fortuneCount = Collections.frequency(sets, EmojiSet.FORTUNE)
        int giftCount = Collections.frequency(sets, EmojiSet.GIFT)
        int moonCount = Collections.frequency(sets, EmojiSet.MOON)
        int peachCount = Collections.frequency(sets, EmojiSet.PEACH)
        int treeCount = Collections.frequency(sets, EmojiSet.TREE)

        then:
        commonEmojiSetCount == 8
        testCount == 0
        blueCount > portionSize * 0.9
        blueCount < portionSize * 1.1
        breadCount > portionSize * 0.9
        breadCount < portionSize * 1.1
        donutCount > portionSize * 0.9
        donutCount < portionSize * 1.1
        fortuneCount > portionSize * 0.9
        fortuneCount < portionSize * 1.1
        giftCount > portionSize * 0.9
        giftCount < portionSize * 1.1
        moonCount > portionSize * 0.9
        moonCount < portionSize * 1.1
        peachCount > portionSize * 0.9
        peachCount < portionSize * 1.1
        treeCount > portionSize * 0.9
        treeCount < portionSize * 1.1
    }
}
