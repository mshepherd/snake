package sondow.snake.conf

import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables
import spock.lang.Specification
import twitter4j.conf.Configuration

class GameConfigFactorySpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    def "configure should populate a configuration with environment variables"() {
        setup:
        String filler = Environment.SPACE_FILLER
        envVars.set("CRED_AWS_ACCESS_KEY", "123")
        envVars.set("CRED_AWS_SECRET_KEY", "456")
        envVars.set("twitter_account", "cartoons")
        envVars.set("cartoons_twitter4j_oauth_accessToken", "${filler}fredflintstone")
        envVars.set("cartoons_twitter4j_oauth_accessTokenSecret", "${filler}bugsbunny")
        envVars.set("CRED_AIRTABLE_API_KEY",
                "${filler}${filler}${filler}daffy${filler}${filler}${filler}")
        envVars.set("CRED_AIRTABLE_BASE",
                "${filler}${filler}${filler}sylvester${filler}${filler}${filler}")

        when:
        GameConfig gameConfig = new GameConfigFactory().configure()
        Configuration twitterConfig = gameConfig.getTwitterConfig()

        then:
        with(gameConfig) {
            airtableApiKey == 'daffy'
            airtableBaseId == 'sylvester'
        }
        with(twitterConfig) {
            OAuthAccessToken == 'fredflintstone'
            OAuthAccessTokenSecret == 'bugsbunny'
        }
    }

    def "configure should populate a configuration with account specific env vars"() {
        setup:
        String filler = Environment.SPACE_FILLER
        envVars.set("CRED_AWS_ACCESS_KEY", "123")
        envVars.set("CRED_AWS_SECRET_KEY", "456")
        envVars.set("twitter_account", "picardtips")
        envVars.set("picardtips_twitter4j_oauth_accessToken", "${filler}therearefourlight")
        envVars.set("picardtips_twitter4j_oauth_accessTokenSecret", "${filler}engage")
        envVars.set("rikergoogling_twitter4j_oauth_accessToken", "${filler}jazz")
        envVars.set("rikergoogling_twitter4j_oauth_accessTokenSecret", "${filler}beard")
        envVars.set("CRED_AIRTABLE_API_KEY",
                "${filler}${filler}${filler}daffy${filler}${filler}${filler}")
        envVars.set("CRED_AIRTABLE_BASE",
                "${filler}${filler}${filler}sylvester${filler}${filler}${filler}")

        when:
        Configuration config = new GameConfigFactory().configure().getTwitterConfig()

        then:
        with(config) {
            OAuthAccessToken == 'therearefourlight'
            OAuthAccessTokenSecret == 'engage'
        }
    }
}
