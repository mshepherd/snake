package sondow.snake.game;

/**
 * Indicator of whether the game is still running, or has been won, or has been lost.
 */
public enum Mode {
    RUNNING, WON, LOST
}
