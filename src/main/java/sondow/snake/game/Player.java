package sondow.snake.game;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import sondow.snake.conf.GameConfig;
import sondow.snake.conf.GameConfigFactory;
import sondow.snake.conf.Time;
import sondow.snake.io.Abridger;
import sondow.snake.io.AirtableDatabase;
import sondow.snake.io.Converter;
import sondow.snake.io.Database;
import sondow.snake.io.EmojiSet;
import sondow.snake.io.Outcome;
import sondow.snake.io.Referee;
import sondow.snake.io.TwitterPollMaker;
import twitter4j.Card;
import twitter4j.Status;
import twitter4j.StatusWithCard;
import static java.time.temporal.ChronoUnit.SECONDS;

/**
 * Executes a game turn from start to finish.
 */
public class Player {

    private GameConfig gameConfig;
    private Database database;
    private Random random;
    private TwitterPollMaker pollMaker;
    private String twitterHandle;
    private Time time;
    private EmojiSet emojiSet;

    public Player(Random random, Time time, Database database,
            TwitterPollMaker pollMaker,
            EmojiSet emojiSet) {
        init(random, time, emojiSet);
        this.database = database;
        this.pollMaker = pollMaker;
    }

    public Player() {
        Random random = new Random();
        init(random, new Time(), EmojiSet.pickOne(random));
        this.database = new AirtableDatabase(gameConfig);
        this.pollMaker = new TwitterPollMaker(gameConfig.getTwitterConfig());
    }

    public static void main(String[] args) {
        Player player = new Player();
        player.play();
    }

    private void init(Random random, Time time, EmojiSet emojiSet) {
        this.gameConfig = new GameConfigFactory().configure();
        this.twitterHandle = gameConfig.getTwitterConfig().getUser();
        this.random = random;
        this.time = time;
        this.emojiSet = emojiSet;
    }

    public Outcome play() {
        String stateJson = database.readGameState();
        System.out.println("Read game state from database: " + stateJson);
        Converter converter = new Converter(random);
        Game game;
        String previousGameString = null;
        if (stateJson == null) {
            game = new Game(emojiSet, random);
            game.spawnSnake();
            game.spawnTarget();
        } else {
            game = converter.makeGameFromJson(stateJson);
            previousGameString = game.toString();
        }
        Outcome outcome = readPreviousPollAndPostNewTweets(game, false);
        outcome.setPreviousGameString(previousGameString);
        int maxRetries = 6;
        for (int i = 1; i <= maxRetries && outcome.getRetryDelaySeconds() >= 1; i++) {
            System.out.println("retry " + i + ", outcome.getRetryDelaySeconds(): " + outcome
                    .getRetryDelaySeconds());
            waitForPollEnd(outcome.getRetryDelaySeconds());
            // Sometimes Twitter takes way too long to process finishing a poll, and I don't want
            // to wait indefinitely for that opaque and unreliable process to finish.
            boolean giveUpAndDoItAnyway = (i >= maxRetries);
            outcome = readPreviousPollAndPostNewTweets(game, giveUpAndDoItAnyway);
        }

        List<Status> tweets = outcome.getTweets();
        if (tweets != null && tweets.size() >= 1) {
            Status lastTweet = tweets.get(tweets.size() - 1);
            game.setTweetId(lastTweet.getId());
            game.setThreadLength(game.getThreadLength() + tweets.size());
            if (game.isGameOver()) {
                database.deleteGameState();
            } else {
                String jsonFromGame = converter.makeJsonFromGame(game);
                System.out.println("Write game state to database: " + jsonFromGame);
                database.writeGameState(jsonFromGame);
            }
        }
        return outcome;
    }

    private Outcome readPreviousPollAndPostNewTweets(Game game, boolean giveUpAndDoItAnyway) {
        Outcome outcome = new Outcome();
        Integer turnLengthMinutes = gameConfig.getTurnLengthMinutes();
        Long tweetId = game.getTweetId();
        StatusWithCard previous = pollMaker.readPreviousTweet(tweetId);
        if (previous == null || shouldAssessResultsNow(giveUpAndDoItAnyway, outcome, previous)) {
            List<Status> tweets = postTweets(game, turnLengthMinutes, pollMaker, previous);
            outcome.setTweets(tweets);
        }
        return outcome;
    }

    private boolean shouldAssessResultsNow(boolean giveUpAndDoItAnyway, Outcome
            outcome, StatusWithCard previousTweet) {

        Card poll = previousTweet.getCard();
        boolean countsAreFinal = poll.isCountsAreFinal();
        System.out.println("are poll counts final? " + countsAreFinal);
        boolean assessResultsNow;
        if (giveUpAndDoItAnyway || countsAreFinal) {
            assessResultsNow = true;
        } else {
            Referee referee = new Referee();
            if (referee.hasSuperMajority(poll)) {
                assessResultsNow = true;
            } else {
                long tweetId = previousTweet.getId();
                long pollTimeSecondsRemaining = calculatePollSecondsRemaining(poll);
                if (pollTimeSecondsRemaining > 60) {
                    throw new RuntimeException("Close race ends much later. secondsRemaining=" +
                            pollTimeSecondsRemaining + " tweetId=" + tweetId);
                }
                outcome.setRetryDelaySeconds(pollTimeSecondsRemaining + 5);
                assessResultsNow = false;
            }
        }
        return assessResultsNow;
    }

    private List<Status> postTweets(Game game, Integer turnLengthMinutes,
            TwitterPollMaker pollMaker, StatusWithCard previousGameTweet) {
        Referee referee = new Referee();
        Long idToReplyTo = null;
        boolean timeToBreakThread = (game.getThreadLength() >= 50);
        List<GameSummary> gameSummaries = new ArrayList<>();
        if (previousGameTweet == null) {
            // New game. Start from scratch.
            String text = game.toString();
            gameSummaries.add(new GameSummary(text, game.getSnake().getAtomicMovesSinceEating()));
        } else {
            final long previousGameTweetId = previousGameTweet.getId();
            idToReplyTo = previousGameTweetId; // If continuing thread
            Move move = referee.determineElectedMove(previousGameTweet);
            if (move != null) {
                try {
                    gameSummaries.addAll(game.doCompositeMove(move));
                } catch (GameOverException e) {
                    System.out.println("Game over with score " + game.getScore());
                }
            }

            if (timeToBreakThread) {
                // Start a new thread.

                // Three new tweets.

                // 1. newThreadIntroTweet: In reply to null, the start of the new thread should
                // be a tweet, possibly with a short message explaining that we're continuing a
                // game from another thread, and a link back to the last game tweet of the old
                // thread.
                // 2. newGameTweet: In reply to tweet 1, newThreadIntroTweet, make the
                // newGameTweet.
                // 3. oldThreadOutroTweet: In reply to the last game tweet of the old thread, a
                // tweet containing a link to tweet 2, newGameTweet.
                String previousGameTweetUrl = buildTweetUrl(previousGameTweetId);
                String newThreadIntroText = "Continuing game from thread… " + previousGameTweetUrl;
                Status newThreadIntroTweet = pollMaker.tweet(newThreadIntroText, null, null);
                idToReplyTo = newThreadIntroTweet.getId();
                time.pauseBriefly(); // Avoid tweeting too rapidly
            }
        }

        List<Status> newTweets = new ArrayList<>();

        // Assigned null for compilation. In practice, the for loop should always have at least
        // one iteration.
        Status newTweet = null;
        Abridger abridger = new Abridger();
        List<GameSummary> abridgedGameSummaries = abridger.abridge(gameSummaries);
        assert abridgedGameSummaries.size() >= 1;

        // Iterate through the gameRenderings. Assume that the last rendering will be either a game
        // poll tweet or a game over screen.
        for (int i = 0; i < abridgedGameSummaries.size(); i++) {
            if (i > 0) {
                time.pauseBriefly(); // Avoid tweeting too rapidly
            }
            GameSummary gameSummary = abridgedGameSummaries.get(i);
            String text = gameSummary.getGameRendering();
            boolean hasPoll = true;
            // If it's not the last one in the list then expect it's an intermediary with no poll
            if (i <= abridgedGameSummaries.size() - 2) {
                hasPoll = false;
            } else if (game.isGameOver()) {
                hasPoll = false;
            }
            if (hasPoll) {
                List<String> choices = Move.toPollChoices(game.listLegalMoves());
                newTweet = pollMaker.postPoll(turnLengthMinutes, text, choices, idToReplyTo);
            } else {
                // This is an intermediary progress tweet or a game over tweet
                newTweet = pollMaker.tweet(text, idToReplyTo, null);
            }
            idToReplyTo = newTweet.getId();
            newTweets.add(newTweet);
        }

        if (previousGameTweet != null && timeToBreakThread) {

            String newGameTweetUrl = buildTweetUrl(newTweet.getId());
            time.pauseBriefly(); // Avoid tweeting too rapidly
            pollMaker.tweet("Game continues in new thread… " + newGameTweetUrl,
                    previousGameTweet.getId(), null);

            game.setThreadLength(0);
        }

        return newTweets;
    }

    private String buildTweetUrl(long id) {
        return "https://twitter.com/" + twitterHandle + "/status/" + id;
    }

    private long calculatePollSecondsRemaining(Card poll) {
        Instant endDatetimeUtc = poll.getEndDatetimeUtc();
        Instant now = Instant.now();
        long pollTimeSecondsRemaining = Math.max(0, now.until(endDatetimeUtc, SECONDS));
        System.out.println("now = " + now + ", endDatetimeUtc = " + endDatetimeUtc +
                ", pollTimeSecondsRemaining = " + pollTimeSecondsRemaining);
        return pollTimeSecondsRemaining;
    }

    private void waitForPollEnd(long secondsToWait) {
        try {
            System.out.println("Waiting " + secondsToWait + " seconds.");
            Thread.sleep(secondsToWait * 1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
