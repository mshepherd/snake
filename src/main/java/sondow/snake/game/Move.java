package sondow.snake.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum Move {
    UP("⬆️ Up", "up"),
    LEFT("⬅️ Left", "lf"),
    RIGHT("➡️ Right", "ri"),
    DOWN("⬇️ Down", "dn");

    private static Map<String, Move> EMOJI_AND_TITLE_CASE_TO_MOVE = new HashMap<>();
    private static Map<String, Move> ABBREVIATION_TO_MOVE = new HashMap<>();

    public static final List<Move> UP_LEFT_RIGHT_DOWN = Arrays.asList(UP, LEFT, RIGHT, DOWN);

    static {
        for (Move move : Move.values()) {
            ABBREVIATION_TO_MOVE.put(move.getAbbreviation(), move);
        }

        for (Move move : Move.values()) {
            EMOJI_AND_TITLE_CASE_TO_MOVE.put(move.getEmojiAndTitleCase(), move);
        }
    }

    private final String emojiAndTitleCase;
    private final String abbreviation;

    Move(String emoji, String abbreviation) {
        this.emojiAndTitleCase = emoji;
        this.abbreviation = abbreviation;
    }

    /**
     * Tries to find a matching Move for a given emoji and title case string.
     *
     * @param emojiAndTitleCase the string to look up
     * @return the matching Move
     */
    public static Move fromEmojiAndTitleCase(String emojiAndTitleCase) {
        return EMOJI_AND_TITLE_CASE_TO_MOVE.get(emojiAndTitleCase);
    }

    /**
     * Tries to find a matching Move for a given abbreviation string, probably from database.
     *
     * @param abbreviation the abbreviation to look up
     * @return the matching Move
     */
    public static Move fromAbbreviation(String abbreviation) {
        return ABBREVIATION_TO_MOVE.get(abbreviation);
    }

    /**
     * Poll choice strings for moves show an emojiAndTitleCase, followed by a space, followed by
     * the title case version of the move name.
     *
     * @param moves the moves to convert
     * @return the list of string representations of the moves, with emojis included
     */
    public static List<String> toPollChoices(List<Move> moves) {
        List<String> choices = new ArrayList<>();
        for (Move move : moves) {
            choices.add(move.getEmojiAndTitleCase());
        }
        return choices;
    }

    String getEmojiAndTitleCase() {
        return emojiAndTitleCase;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
}
