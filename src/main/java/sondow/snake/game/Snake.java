package sondow.snake.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A snake has a series of key points, which are the head, the points where the snake bends, and
 * tail end. This class can also generate a full list of all points including key points and
 * in-between points, in order from head to tail.
 */
public class Snake {

    private List<Point> keyPoints = new ArrayList<>();
    private int atomicMovesSinceEating = 0;

    public int getAtomicMovesSinceEating() {
        return atomicMovesSinceEating;
    }

    public Snake setAtomicMovesSinceEating(int atomicMovesSinceEating) {
        this.atomicMovesSinceEating = atomicMovesSinceEating;
        return this;
    }

    public List<Point> getKeyPoints() {
        return keyPoints;
    }

    public Snake setKeyPoints(List<Point> keyPoints) {
        this.keyPoints = keyPoints;
        return this;
    }

    /**
     * Adds a key point to the snake at the specified grid coordinates. Key points are the start,
     * end, and corners of the snake where it bends.
     *
     * @param point the coordinates of the point to add
     * @return the snake, for method chain
     */
    public Snake addKeyPoint(Point point) {
        return addKeyPoint(point.getRow(), point.getCol());
    }

    /**
     * Adds a key point to the snake at the specified grid coordinates. Key points are the start,
     * end, and corners of the snake where it bends.
     *
     * @param row the row index
     * @param col the column index
     * @return the snake, for method chain
     */
    public Snake addKeyPoint(int row, int col) {
        Point current = new Point(row, col);

        // Enforce rules for where segment can be in relation to previous segment.

        // Is there a previous segment?
        if (keyPoints.size() >= 1) {
            Point previous = keyPoints.get(keyPoints.size() - 1);
            assertKeyPointsAreAligned(previous, current);
        }

        keyPoints.add(current);
        return this;
    }

    /**
     * @return the full list of all snake points including key points and in-between points, in
     * order from snake head to snake tail
     */
    public List<Point> getAllPoints() {
        List<Point> allPoints = getAllPointsExceptTailEnd();
        Point tailEnd = getTailEnd();
        allPoints.add(tailEnd);
        return allPoints;
    }

    public Point getHead() {
        return keyPoints.get(0);
    }

    public Point getTailEnd() {
        return keyPoints.get(keyPoints.size() - 1);
    }

    /**
     * @return the list of all snake points including key points and in-between points, in order
     * from snake head to snake tail, but with the exclusion of the final tail end point
     */
    public List<Point> getAllPointsExceptTailEnd() {
        return interpolatePointsExceptTailEnd(1);
    }

    /**
     * @return the list of interpolated snake points including key points and in-between points
     * (limited in proximity to each other by the stepCount), in order from snake head to snake
     * tail, but with the exclusion of the final tail end point
     */
    private List<Point> interpolatePointsExceptTailEnd(int stepCount) {
        List<Point> allPoints = new ArrayList<>();
        if (keyPoints.size() <= 1) {
            throw new RuntimeException("Invalid snake data. Not enough key points: " + keyPoints);
        }
        for (int i = 1; i < keyPoints.size(); i++) {
            Point previous = keyPoints.get(i - 1);
            allPoints.add(previous);
            Point current = keyPoints.get(i);
            // Do previous and current share a row or a column?
            int previousRow = previous.getRow();
            int currentRow = current.getRow();
            int previousCol = previous.getCol();
            int currentCol = current.getCol();
            if (previousCol == currentCol) {
                // Create the intermediary segments.
                int diff = previousRow - currentRow;
                if (diff >= stepCount) {
                    // Decrement previous until you reach current.
                    int tweenRow = previousRow - stepCount;
                    while (tweenRow > currentRow) {
                        Point tween = new Point(tweenRow, previousCol);
                        allPoints.add(tween);
                        tweenRow -= stepCount;
                    }
                } else if (diff <= -1 * stepCount) {
                    // Increment previous until you reach current.
                    int tweenRow = previousRow + stepCount;
                    while (tweenRow < currentRow) {
                        Point tween = new Point(tweenRow, previousCol);
                        allPoints.add(tween);
                        tweenRow += stepCount;
                    }
                }
            } else if (previousRow == currentRow) {
                // Create the intermediary segments.
                int diff = previousCol - currentCol;
                if (diff >= stepCount) {
                    // Decrement previous until you reach current.
                    int tweenCol = previousCol - stepCount;
                    while (tweenCol > currentCol) {
                        Point tween = new Point(previousRow, tweenCol);
                        allPoints.add(tween);
                        tweenCol -= stepCount;
                    }
                } else if (diff <= -1 * stepCount) {
                    // Increment previous until you reach current.
                    int tweenCol = previousCol + stepCount;
                    while (tweenCol < currentCol) {
                        Point tween = new Point(previousRow, tweenCol);
                        allPoints.add(tween);
                        tweenCol += stepCount;
                    }
                }
            }
        }
        return allPoints;
    }

    /**
     * Adds keypoints to snake at all intersection points (even-numbered coordinates) along snake.
     *
     * @return this snake, for method chaining
     */
    public List<Point> interpolateIntersectionPoints() {
        List<Point> intersectionPoints = interpolatePointsExceptTailEnd(2);
        Point tailEnd = getTailEnd();
        intersectionPoints.add(tailEnd);
        return intersectionPoints;
    }

    /**
     * Removes all interpolated intersection points from the list of key points, leaving only the
     * bare minimum of end and bend points.
     *
     * @return this snake, for method chaining
     */
    public Snake extrapolateKeyPoints() {
        List<Point> extrapolatedKeyPoints = new ArrayList<>();
        List<Point> currentKeyPoints = getKeyPoints();
        Move prevDirection = null;
        Point head = currentKeyPoints.get(0);
        extrapolatedKeyPoints.add(head);
        for (int i = 1; i < currentKeyPoints.size(); i++) {
            Point current = currentKeyPoints.get(i);
            Point previous = currentKeyPoints.get(i - 1);
            Move direction = calculateDirection(current, previous);

            // Does the current segment direction differ from the previous direction?
            if (prevDirection != null && direction != prevDirection) {
                extrapolatedKeyPoints.add(previous);
            }
            prevDirection = direction;
        }
        Point tailEnd = currentKeyPoints.get(currentKeyPoints.size() - 1);
        extrapolatedKeyPoints.add(tailEnd);
        setKeyPoints(extrapolatedKeyPoints);
        return this;
    }

    private Move calculateDirection(Point current, Point previous) {
        int curRow = current.getRow();
        int curCol = current.getCol();
        int prevRow = previous.getRow();
        int prevCol = previous.getCol();

        Move direction = null;
        // What direction is the cursor moving?
        if (curRow < prevRow) {
            direction = Move.UP;
        } else if (curRow > prevRow) {
            direction = Move.DOWN;
        } else if (curCol < prevCol) {
            direction = Move.LEFT;
        } else if (curCol > prevCol) {
            direction = Move.RIGHT;
        } else {
            throw new RuntimeException("There shouldn't be two identical key points in snake");
        }
        return direction;
    }

    /**
     * Figure out if previous and current share a row or a column. They must share exactly one of
     * those fields, not zero, not two.
     *
     * @param previous the previous segment
     * @param current  the current segment
     */
    private void assertKeyPointsAreAligned(Point previous, Point current) {
        int prevCol = previous.getCol();
        int prevRow = previous.getRow();
        int curCol = current.getCol();
        int curRow = current.getRow();
        if (prevCol == curCol && prevRow == curRow) {
            throw new RuntimeException(
                    "Invalid snake data. Multiple key points at row " + prevRow + " column " +
                            prevCol);
        } else if (prevCol != curCol && prevRow != curRow) {
            throw new RuntimeException("Invalid snake data. Key " + previous +
                    " does not share a row or column with next key " + current);
        }
    }

    public Snake deepCopy() {
        List<Point> keyPointsCopy = Arrays.asList(this.getKeyPoints().toArray(new Point[0]));
        int sinceEating = this.getAtomicMovesSinceEating();
        return new Snake().setAtomicMovesSinceEating(sinceEating).setKeyPoints(keyPointsCopy);
    }

    @Override public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        Snake snake = (Snake) o;

        if (getAtomicMovesSinceEating() != snake.getAtomicMovesSinceEating()) { return false; }
        return getKeyPoints() != null ? getKeyPoints().equals(snake.getKeyPoints()) :
                snake.getKeyPoints() == null;
    }

    @Override public int hashCode() {
        int result = getKeyPoints() != null ? getKeyPoints().hashCode() : 0;
        result = 31 * result + getAtomicMovesSinceEating();
        return result;
    }
}
