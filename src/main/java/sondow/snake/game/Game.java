package sondow.snake.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import sondow.snake.io.EmojiSet;
import static sondow.snake.game.Move.DOWN;
import static sondow.snake.game.Move.LEFT;
import static sondow.snake.game.Move.RIGHT;
import static sondow.snake.game.Move.UP;
import static sondow.snake.game.Move.UP_LEFT_RIGHT_DOWN;

/**
 * The game grid and current snake and target and related functionality.
 *
 * @author @JoeSondow
 */
public class Game {

    /**
     * Number of rows in the grid of tile spaces
     */
    private final int rowCnt = 11;

    /**
     * Number of columns in the grid of tile spaces
     */
    private final int colCnt = 9;

    /**
     * This is 2 as long as the game grid has wall spacers for readability.
     */
    static final int POINTS_TO_ADJACENT_INTERSECTION = 2;

    private final EmojiSet emojiSet;

    /**
     * Flag for Game Over status
     */
    private Mode mode = Mode.RUNNING;
    //    private boolean gameOver = false;
    private int score;
    private Snake snake;
    private Target target;
    private Random random;
    private Long tweetId;

    /**
     * Number of tweets since new twitter thread started.
     */
    private int threadLength;

    /**
     * List of all intersection points on the grid, where targets and snake key points can go.
     */
    private List<Point> allIntersectionPoints;

    /**
     * Instantiates a game grid.
     */
    public Game(EmojiSet emojiSet, Random random) {
        this.emojiSet = emojiSet;
        this.random = random;
    }

    public void scoreEatenTarget(int atomicMovesSinceEating) {
        // 120 points minus 10x the number of atomic moves done since target spawn, max 120, min 0
        int modifier = Math.min(Math.max(atomicMovesSinceEating, 0), 12);
        int pointsToAdd = (12 - modifier) * 10;
        setScore(getScore() + pointsToAdd);
    }

    private List<Point> listAllIntersectionPoints() {
        if (allIntersectionPoints == null) {
            allIntersectionPoints = new ArrayList<>();
            for (int r = 0; r < rowCnt; r += 2) {
                for (int c = 0; c < colCnt; c += 2) {
                    Point p = new Point(r, c);
                    allIntersectionPoints.add(p);
                }
            }
        }
        return allIntersectionPoints;
    }

    /**
     * Creates a new snake starting at a random intersection point.
     */
    public void spawnSnake() {
        List<Point> intersectionPoints = listAllIntersectionPoints();
        int i = random.nextInt(intersectionPoints.size());
        Point head = intersectionPoints.get(i);
        // List which directions the cursor can move in to pick a tail point.
        List<Move> possibleDirections = new ArrayList<>();
        if (head.getRow() > 0) {
            possibleDirections.add(UP);
        }
        if (head.getCol() > 0) {
            possibleDirections.add(LEFT);
        }
        if (head.getRow() < rowCnt - 1) {
            possibleDirections.add(DOWN);
        }
        if (head.getCol() < colCnt - 1) {
            possibleDirections.add(RIGHT);
        }
        Snake snake = new Snake().addKeyPoint(head);
        // Pick a direction
        Move move = possibleDirections.get(random.nextInt(possibleDirections.size()));
        // Move the snake-drawing cursor in the chosen direction to choose the tail point.
        Point nextPoint = moveToAdjacentPoint(head, move);
        snake.addKeyPoint(nextPoint); // Tail of short snake
        setSnake(snake);
    }

    private Point moveToAdjacentPoint(Point start, Move move) {
        int row = start.getRow();
        int col = start.getCol();
        if (move == UP) {
            row -= POINTS_TO_ADJACENT_INTERSECTION;
        } else if (move == DOWN) {
            row += POINTS_TO_ADJACENT_INTERSECTION;
        } else if (move == LEFT) {
            col -= POINTS_TO_ADJACENT_INTERSECTION;
        } else if (move == RIGHT) {
            col += POINTS_TO_ADJACENT_INTERSECTION;
        }
        return new Point(row, col);
    }

    /**
     * Creates a new target at a random unoccupied intersection point.
     */
    public void spawnTarget() {

        List<Point> unoccupiedIntersectionPoints = getUnoccupiedIntersectionPoints();
        int pointCount = unoccupiedIntersectionPoints.size();
        if (pointCount >= 1) {
            int i = random.nextInt(pointCount);
            Point spawnPoint = unoccupiedIntersectionPoints.get(i);
            Target target = new Target(spawnPoint.getRow(), spawnPoint.getCol());
            setTarget(target);
        }
    }

    private List<Point> getUnoccupiedIntersectionPoints() {
        List<Point> allSnakePoints = getSnake().getAllPoints();
        List<Point> allIntersectionPoints = listAllIntersectionPoints();
        List<Point> unoccupiedIntersectionPoints = new ArrayList<>();
        for (Point point : allIntersectionPoints) {
            if (!allSnakePoints.contains(point)) {
                unoccupiedIntersectionPoints.add(point);
            }
        }
        return unoccupiedIntersectionPoints;
    }

    /**
     * Renders the current contents of the game grid.
     */
    @Override public String toString() {
        Grid grid = constructGridWithSnakeAndTarget();
        StringBuilder builder = new StringBuilder();

        List<String> rowsAsStrings = grid.getRowsAsStrings();
        builder.append("Score ").append(getScore()).append("\n\n");
        for (int i = 0; i < rowsAsStrings.size(); i++) {
            if (i > 0) {
                builder.append("\n");
            }
            String row = rowsAsStrings.get(i);
            builder.append(row);
        }
        return builder.toString();
    }

    private Grid constructGridWithSnakeAndTarget() {
        Grid grid = new Grid(rowCnt, colCnt, emojiSet.getBlank());

        // Put walls and clock at locations with odd-numbered row and column indices.

        Point clock = positionClock();
        for (int r = 1; r < rowCnt; r += POINTS_TO_ADJACENT_INTERSECTION) {
            for (int c = 1; c < colCnt; c += POINTS_TO_ADJACENT_INTERSECTION) {
                String pillar = getWallEmoji(r, c, clock);
                grid.put(r, c, pillar);
            }
        }
        Target target = getTarget();
        if (target != null) {
            String targetEmoji = isLost() ? emojiSet.getLoserTarget() : emojiSet.getTarget();
            grid.put(target.getRow(), target.getCol(), targetEmoji);
        }
        Snake snake = getSnake();
        if (snake != null) {
            List<Point> allPoints = snake.getAllPoints();

            Point head = allPoints.get(0);
            int atomicMovesSinceEating = snake.getAtomicMovesSinceEating();
            String headEmoji;
            if (isLost()) {
                headEmoji = emojiSet.getLoserHead();
            } else if (isWon()) {
                headEmoji = emojiSet.getWinnerHead();
            } else if (target != null && target.isAdjacentTo(head)) {
                headEmoji = emojiSet.getExcitedHead();
            } else if (atomicMovesSinceEating <= 0) {
                headEmoji = emojiSet.getSatedHead();
            } else if (atomicMovesSinceEating <= 4) {
                headEmoji = emojiSet.getContentHead();
            } else if (atomicMovesSinceEating <= 8) {
                headEmoji = emojiSet.getNeutralHead();
            } else {
                headEmoji = emojiSet.getStarvingHead();
            }
            grid.put(head.getRow(), head.getCol(), headEmoji);
            for (int i = 1; i < allPoints.size(); i++) {
                Point point = allPoints.get(i);
                String body = emojiSet.getBody();
                if (isLost()) {
                    body = emojiSet.getLoserBody();
                } else if (isWon()) {
                    body = emojiSet.getWinnerBody();
                }
                grid.put(point.getRow(), point.getCol(), body);
            }
        }
        return grid;
    }

    private Point positionClock() {
        Point clock = null;
        Target target = getTarget();
        if (target != null) {
            int targetRow = target.getRow();
            int targetCol = target.getCol();

            // If the target is in the top right corner, then the clock is on the apple's lower
            // left.
            // If the target is at the top edge, then the clock is on the apple's lower right.
            // If the target is on the right edge, then the clock is on the apple's upper left.
            // Otherwise the clock is in upper right of target.
            int clockRow;
            if (targetRow == 0) {
                clockRow = 1;
            } else {
                clockRow = targetRow - 1;
            }
            int clockCol;
            if (targetCol == colCnt - 1) {
                clockCol = targetCol - 1;
            } else {
                clockCol = targetCol + 1;
            }
            clock = new Point(clockRow, clockCol);
        }
        return clock;
    }

    private String getWallEmoji(int row, int col, Point clock) {
        String pillar = emojiSet.getWall();
        if (isWon()) {
            if (row == 3) {
                if (col == 1) {
                    pillar = "🇬";
                } else if (col == 3) {
                    pillar = "🇴";
                } else if (col == 5) {
                    pillar = "🇴";
                } else if (col == 7) {
                    pillar = "🇩";
                }
            } else if (row == 5) {
                if (col == 1) {
                    pillar = "🇬";
                } else if (col == 3) {
                    pillar = "🇦";
                } else if (col == 5) {
                    pillar = "🇲";
                } else if (col == 7) {
                    pillar = "🇪";
                }
            }
        } else if (isLost()) {
            if (row == 3) {
                if (col == 1) {
                    pillar = "🇬";
                } else if (col == 3) {
                    pillar = "🇦";
                } else if (col == 5) {
                    pillar = "🇲";
                } else if (col == 7) {
                    pillar = "🇪";
                }
            } else if (row == 5) {
                if (col == 1) {
                    pillar = "🇴";
                } else if (col == 3) {
                    pillar = "🇻";
                } else if (col == 5) {
                    pillar = "🇪";
                } else if (col == 7) {
                    pillar = "🇷";
                }
            }
        } else if (clock != null && clock.getRow() == row && clock.getCol() == col) {
            pillar = getClockEmoji();
        }
        return pillar;
    }

    private String getClockEmoji() {
        int moves = getSnake().getAtomicMovesSinceEating();
        int clockHour = 12 - moves;
        emojiSet.getClock(clockHour);
        return emojiSet.getClock(clockHour);
    }

    /**
     * Checks whether the snake can move in a specified direction.
     *
     * @param move the move to consider
     * @return true if the piece can legally perform the specified move
     */
    private boolean canMove(Move move) {
        return canDoSingleMove(move);
    }

    private boolean isOffGrid(Point point) {
        int row = point.getRow();
        int col = point.getCol();
        return row < 0 || row > rowCnt - 1 || col < 0 || col > colCnt - 1;
    }

    /**
     * Checks whether the snake can legally perform the specified move.
     *
     * @param move the move to consider
     * @return true if the piece can legally perform the specified move, false otherwise
     */
    private boolean canDoSingleMove(Move move) {

        // Calculate which point the snake head would go to if this move were executed. If that
        // point is occupied by a snake body point, or is off the grid, then return false.
        // Otherwise, true.

        // The head can go in the direction where the tail end currently is in an adjacent
        // intersection if and only if the snake is longer than three points from head to tail.
        Snake snake = getSnake();
        List<Point> allSnakePointsExceptTailEnd = snake.getAllPointsExceptTailEnd();

        Point head = allSnakePointsExceptTailEnd.get(0);
        Point destination = moveToAdjacentPoint(head, move);
        Point tailEnd = snake.getTailEnd();
        boolean ableToMove = true;
        if (isOffGrid(destination)) {
            ableToMove = false;
        } else if (allSnakePointsExceptTailEnd.contains(destination)) {
            ableToMove = false;
        } else if (destination.equals(tailEnd) && allSnakePointsExceptTailEnd.size() <= 2) {
            ableToMove = false;
        }

        return ableToMove;
    }

    /**
     * Do one or more atomic moves, probably from a player-elected move.
     *
     * @param move the move that may involve one or more atomic moves
     * @return the game string renderings and atomic moves since eating, of all atomic moves done
     */
    public List<GameSummary> doCompositeMove(Move move) {
        List<GameSummary> gameSummaries = new ArrayList<>();
        GameSummary gameSummary = doAtomicMove(move);
        gameSummaries.add(gameSummary);
        List<GameSummary> moreRenderings = doForcedMoves();
        gameSummaries.addAll(moreRenderings);
        return gameSummaries;
    }

    /**
     * Do a move that is the smallest possible move such as Up, Down, Left, Right, traversing only
     * one intersection, which is only two emoji spaces.
     *
     * @param move the move to do
     * @return the game rendering and atomic moves since eating, after this move
     */
    private GameSummary doAtomicMove(Move move) {
        Snake snake = getSnake();
        Point head = snake.getHead();
        List<Point> intersectionPoints = snake.interpolateIntersectionPoints();
        Point tailEnd = intersectionPoints.remove(intersectionPoints.size() - 1);
        Snake newSnake = new Snake();
        Target target = getTarget();
        Point newHead;
        if (move == LEFT) {
            newHead = new Point(head.getRow(), head.getCol() - POINTS_TO_ADJACENT_INTERSECTION);
        } else if (move == RIGHT) {
            newHead = new Point(head.getRow(), head.getCol() + POINTS_TO_ADJACENT_INTERSECTION);
        } else if (move == UP) {
            newHead = new Point(head.getRow() - POINTS_TO_ADJACENT_INTERSECTION, head.getCol());
        } else if (move == DOWN) {
            newHead = new Point(head.getRow() + POINTS_TO_ADJACENT_INTERSECTION, head.getCol());
        } else {
            throw new RuntimeException("Not a valid move: " + move);
        }
        newSnake.addKeyPoint(newHead);

        for (Point point : intersectionPoints) {
            newSnake.addKeyPoint(point);
        }
        boolean eating = target.getRow() == newHead.getRow() && target.getCol() == newHead.getCol();
        int atomicMovesSinceEating = snake.getAtomicMovesSinceEating();
        int newAtomicMovesSinceEating;
        setSnake(newSnake);
        if (eating) {
            scoreEatenTarget(atomicMovesSinceEating);
            newAtomicMovesSinceEating = 0;

            // If eating target, put removed tail end back on, & spawn new target outside new snake.
            newSnake.addKeyPoint(tailEnd);

            // If eating target, then spawn new target somewhere outside of new snake.
            spawnTarget();
        } else {
            newAtomicMovesSinceEating = snake.getAtomicMovesSinceEating() + 1;
        }
        newSnake.setAtomicMovesSinceEating(newAtomicMovesSinceEating);

        // Snake's shape is now fully formed. Remove redundant key points for storage compression.
        newSnake.extrapolateKeyPoints();
        if (getUnoccupiedIntersectionPoints().size() <= 0) {
            // Good game. Show smiles, rainbows, pretty things, happy words
            win();
        } else if (listLegalMoves().size() <= 0) {
            // Show sad failure things.
            lose();
        }
        String text = this.toString();
        return new GameSummary(text, newAtomicMovesSinceEating);
    }

    /**
     * Checks to see if there are meaningful choices for players to make. Performs "Down" moves
     * until either the piece has other options or until the piece can do nothing but "Stop" and
     * spawn the next piece.
     *
     * @return the game string renderings and atomic moves since eating, of all the moves that
     * occur in the loop
     */
    private List<GameSummary> doForcedMoves() {
        List<GameSummary> gameSummaries = new ArrayList<>();
        List<Move> moves = listLegalMoves();
        while (moves.size() == 1) {
            Move nextMove = moves.get(0);
            gameSummaries.add(doAtomicMove(nextMove));
            moves = listLegalMoves();
        }
        return gameSummaries;
    }

    public boolean isGameOver() {
        return mode == Mode.WON || mode == Mode.LOST;
    }

    private boolean isWon() {
        return this.mode == Mode.WON;
    }

    private boolean isLost() {
        return this.mode == Mode.LOST;
    }

    /**
     * Ends the game with all intersections occupied by snake points.
     */
    private void win() {
        setScore(getScore() + 100);
        this.mode = Mode.WON;
    }

    /**
     * Ends the game with one or more intersections unoccupied by snake points.
     */
    private void lose() {
        this.mode = Mode.LOST;
    }

    public int getScore() {
        return score;
    }

    public Game setScore(int score) {
        this.score = score;
        return this;
    }

    public EmojiSet getEmojiSet() {
        return emojiSet;
    }

    public Target getTarget() {
        return target;
    }

    public Game setTarget(Target target) {
        this.target = target;
        return this;
    }

    public Snake getSnake() {
        return snake;
    }

    public Game setSnake(Snake snake) {
        this.snake = snake;
        return this;
    }

    public Long getTweetId() {
        return tweetId;
    }

    public Game setTweetId(Long tweetId) {
        this.tweetId = tweetId;
        return this;
    }

    public List<Move> listLegalMoves() {
        List<Move> legalMoves = new ArrayList<>();
        if (getUnoccupiedIntersectionPoints().size() >= 1) {
            for (Move move : UP_LEFT_RIGHT_DOWN) {
                if (canMove(move)) {
                    legalMoves.add(move);
                }
            }
        }
        return legalMoves;
    }

    public int getThreadLength() {
        return threadLength;
    }

    public Game setThreadLength(int threadLength) {
        this.threadLength = threadLength;
        return this;
    }

    @Override public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        Game game = (Game) o;

        if (getScore() != game.getScore()) { return false; }
        if (getThreadLength() != game.getThreadLength()) { return false; }
        if (getEmojiSet() != game.getEmojiSet()) { return false; }
        if (mode != game.mode) { return false; }
        if (getSnake() != null ? !getSnake().equals(game.getSnake()) : game.getSnake() != null) {
            return false;
        }
        if (getTarget() != null ? !getTarget().equals(game.getTarget()) :
                game.getTarget() != null) {
            return false;
        }
        if (!Objects.equals(random, game.random)) { return false; }
        if (getTweetId() != null ? !getTweetId().equals(game.getTweetId()) :
                game.getTweetId() != null) { return false; }
        return Objects.equals(allIntersectionPoints, game.allIntersectionPoints);
    }

    @Override public int hashCode() {
        int result = (getEmojiSet() != null ? getEmojiSet().hashCode() : 0);
        result = 31 * result + (mode != null ? mode.hashCode() : 0);
        result = 31 * result + getScore();
        result = 31 * result + (getSnake() != null ? getSnake().hashCode() : 0);
        result = 31 * result + (getTarget() != null ? getTarget().hashCode() : 0);
        result = 31 * result + (random != null ? random.hashCode() : 0);
        result = 31 * result + (getTweetId() != null ? getTweetId().hashCode() : 0);
        result = 31 * result + getThreadLength();
        result = 31 * result +
                (allIntersectionPoints != null ? allIntersectionPoints.hashCode() : 0);
        return result;
    }
}
