package sondow.snake.conf;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import twitter4j.conf.Configuration;

public class GameConfig implements AWSCredentialsProvider {

    /**
     * Consumer key for "Twitter for iPhone" a widely known and used app key that is one of the few
     * apps permitted to post and read twitter polls, as of 2018.
     */
    static final String CONSUMER_KEY = "IQKbtAYlXLripLGPWd0HUA";

    /**
     * Consumer Secret for "Twitter for iPhone" a widely known and used app key that is one of the
     * few apps permitted to post and read twitter polls, as of 2018.
     */
    static final String CONSUMER_SECRET = "GgDYlkSvaPxGxC4X8liwpUoqKwwr3lCADbz8A7ADU";

    /**
     * The configuration for Twitter.
     */
    private Configuration twitterConfig;

    /**
     * The configuration for Amazon Web Services (AWS).
     */
    private final AWSCredentials awsCredentials;

    /**
     * The airtable.com account's api key from https://airtable.com/account
     */
    private final String airtableApiKey;

    /**
     * Log into airtable.com and go to https://airtable.com/api to see a list of your bases. Click
     * the base you want to use for this application, and note the ID string in the URL. This is not
     * the name of the base.
     * <p>
     * Example: https://airtable.com/apppPQyZsRLTu0ZCM/api/docs -> base id is apppPQyZsRLTu0ZCM
     */
    private final String airtableBaseId;

    /**
     * The number of minutes between turns, as specified by the TURN_LENGTH_MINUTES environment
     * variable which takes precedence over the length specified in the database, or the default of
     * 20.
     */
    private Integer turnLengthMinutes;

    GameConfig(Configuration twitterConfig, AWSCredentials awsCredentials, Integer
            turnLengthMinutes, String airtableApiKey, String airtableBaseId) {

        this.twitterConfig = twitterConfig;
        this.awsCredentials = awsCredentials;
        this.turnLengthMinutes = turnLengthMinutes;
        this.airtableApiKey = airtableApiKey;
        this.airtableBaseId = airtableBaseId;
    }

    public Configuration getTwitterConfig() {
        return twitterConfig;
    }

    @Override
    public AWSCredentials getCredentials() {
        return awsCredentials;
    }

    public Integer getTurnLengthMinutes() {
        return turnLengthMinutes;
    }

    public String getAirtableApiKey() {
        return airtableApiKey;
    }

    public String getAirtableBaseId() {
        return airtableBaseId;
    }

    @Override
    public void refresh() {
    }
}
