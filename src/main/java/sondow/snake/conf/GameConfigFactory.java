package sondow.snake.conf;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.util.StringUtils;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import static sondow.snake.conf.GameConfig.CONSUMER_KEY;
import static sondow.snake.conf.GameConfig.CONSUMER_SECRET;

public class GameConfigFactory {

    public GameConfig configure() {
        Configuration twitterConf = configureTwitter();
        AWSCredentials awsCreds = getAwsCredentials();
        Integer turnMinutes = Environment.getInt("TURN_LENGTH_MINUTES");
        if (turnMinutes == null) {
            turnMinutes = 20;
        } else if (turnMinutes < 5) {
            throw new RuntimeException("Twitter requires a minimum of 5 minutes for a poll but " +
                    "the TURN_LENGTH_MINUTES environment variable is set to " + turnMinutes);
        }
        String airtableApiKey = Environment.require("CRED_AIRTABLE_API_KEY");
        String airtableBaseId = Environment.require("CRED_AIRTABLE_BASE");
        return new GameConfig(twitterConf, awsCreds, turnMinutes, airtableApiKey, airtableBaseId);
    }

    private AWSCredentials getAwsCredentials() {
        String accessKey = Environment.get("CRED_AWS_ACCESS_KEY");
        String secretKey = Environment.get("CRED_AWS_SECRET_KEY");
        accessKey = StringUtils.trim(accessKey);
        secretKey = StringUtils.trim(secretKey);
        return new BasicAWSCredentials(accessKey, secretKey);
    }

    /**
     * AWS Lambda only allows underscores in environment variables, not dots, so the default ways
     * twitter4j finds keys aren't possible. Instead, this custom code gets the configuration either
     * from Lambda-friendly environment variables or else allows Twitter4J to look in its default
     * locations like twitter4j.properties file at the project root, or on the classpath, or in
     * WEB-INF.
     * <p>
     * The tetraConfig from this method also includes keys for accessing the Twitter API, as well as
     * some domain-specific variables.
     *
     * @return configuration containing AWS and Twitter authentication strings and other variables
     */
    private Configuration configureTwitter() {

        ConfigurationBuilder cb = new ConfigurationBuilder();

        // Override with a specific account if available. This mechanism allows us to provide
        // multiple key sets in the AWS Lambda configuration, and switch which Twitter account
        // to target by retyping just the target Twitter account name in the configuration.
        String account = Environment.require("twitter_account");
        cb.setUser(account);

        String accTokEnvKey = "twitter4j_oauth_accessToken";
        String atsEnvKey = "twitter4j_oauth_accessTokenSecret";

        String accessToken = Environment.either(account + "_" + accTokEnvKey, accTokEnvKey);
        String accessTokenSecret = Environment.either(account + "_" + atsEnvKey, atsEnvKey);

        cb.setOAuthConsumerKey(CONSUMER_KEY);
        cb.setOAuthConsumerSecret(CONSUMER_SECRET);
        cb.setOAuthAccessToken(accessToken);
        cb.setOAuthAccessTokenSecret(accessTokenSecret);

        return cb.setTrimUserEnabled(true).build();
    }

}
