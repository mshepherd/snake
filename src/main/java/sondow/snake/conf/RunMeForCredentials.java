package sondow.snake.conf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

import static sondow.snake.conf.GameConfig.CONSUMER_KEY;
import static sondow.snake.conf.GameConfig.CONSUMER_SECRET;

/**
 * Run the main method of this class on your desktop computer one time in order to acquire
 * credentials for your twitter account to post tweets and interact with the Twitter API using the
 * Twitter for iPhone app which is one of the few blessed apps permitted to post or read Twitter
 * polls, as of 2018.
 * <p>
 * Copy the credentials into environment variables for deployment, or into a twitter4j.properties
 * file if that's your jam. I prefer environment variables, because they're safer (less likely to
 * get committed to a git repo by accident) and because they're easier to change on the fly when
 * you want to direct your AWS Lambda deployment to tweet on a different twitter account, without
 * changing, versioning, rebuilding, and re-uploading your codebase again.
 * <p>
 * Before running this, make sure the twitter4j.properties is currently empty, and that your
 * current default browser is already logged into the Twitter account you want to use.
 */
public class RunMeForCredentials {

    public static void main(String[] args) throws InterruptedException {
        try {
            Twitter twitter = new TwitterFactory().getInstance();
            // These are the credentials for the "Twitter for iPhone" app. They are already
            // publicly available. As of 2018, using these credentials is the only known way to
            // create twitter polls programmatically.
            twitter.setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
            try {
                // get request token.
                // this will throw IllegalStateException if access token is already available
                RequestToken requestToken = twitter.getOAuthRequestToken();
                System.out.println("Got request token.");
                System.out.println("Request token: " + requestToken.getToken());
                System.out.println("Request token secret: " + requestToken.getTokenSecret());
                AccessToken accessToken = null;

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                while (null == accessToken) {
                    System.out.println("Open the following URL and grant access to your account:");
                    System.out.println(requestToken.getAuthorizationURL());
                    System.out.print("Enter the PIN(if available) and hit enter after you granted" +
                            " access.[PIN]:");
                    String pin = br.readLine();
                    try {
                        if (pin.length() > 0) {
                            accessToken = twitter.getOAuthAccessToken(requestToken, pin);
                        } else {
                            accessToken = twitter.getOAuthAccessToken(requestToken);
                        }
                    } catch (TwitterException te) {
                        if (401 == te.getStatusCode()) {
                            System.out.println("Unable to get the access token.");
                        } else {
                            te.printStackTrace();
                        }
                    }
                }
                System.out.println("Got access token.");
                System.out.println("Access token: " + accessToken.getToken());
                System.out.println("Access token secret: " + accessToken.getTokenSecret());
            } catch (IllegalStateException ie) {
                // access token is already available, or consumer key/secret is not set.
                if (!twitter.getAuthorization().isEnabled()) {
                    System.out.println("OAuth consumer key/secret is not set.");
                    System.exit(-1);
                }
            }
            System.exit(0);
        } catch (TwitterException te) {
            te.printStackTrace();
            System.out.println("Failed to get timeline: " + te.getMessage());
            System.exit(-1);
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println("Failed to read the system input.");
            System.exit(-1);
        }

    }
}
