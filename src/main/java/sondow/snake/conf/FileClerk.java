package sondow.snake.conf;

import java.io.File;

public class FileClerk {

    public File getFile(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        String filePath = classLoader.getResource(fileName).getFile();
        return new File(filePath);
    }
}
