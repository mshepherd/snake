package sondow.snake.conf;

import java.io.File;

public class TweetContent {
    private String message;
    private File file;

    public TweetContent(String message, File file) {
        this.message = message;
        this.file = file;
    }

    public String getMessage() {
        return message;
    }

    public File getFile() {
        return file;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TweetContent that = (TweetContent) o;

        if (message != null ? !message.equals(that.message) : that.message != null) {
            return false;
        }
        return file != null ? file.equals(that.file) : that.file == null;
    }

    @Override
    public int hashCode() {
        int result = message != null ? message.hashCode() : 0;
        result = 31 * result + (file != null ? file.hashCode() : 0);
        return result;
    }

}
