package sondow.snake.io;

import java.util.List;
import twitter4j.Status;

/**
 * The outcome of executing the whole program, mostly for the purposes of logging.
 */
public class Outcome {

    private List<Status> tweets;
    private long retryDelaySeconds;
    private String previousGameString;

    public Outcome() {
    }

    public List<Status> getTweets() {
        return tweets;
    }

    public void setTweets(List<Status> tweets) {
        this.tweets = tweets;
    }

    public long getRetryDelaySeconds() {
        return retryDelaySeconds;
    }

    public void setRetryDelaySeconds(long retryDelaySeconds) {
        this.retryDelaySeconds = retryDelaySeconds;
    }

    public String getPreviousGameString() {
        return previousGameString;
    }

    public void setPreviousGameString(String previousGameString) {
        this.previousGameString = previousGameString;
    }

    @Override public String toString() {
        return "Outcome{" +
                "tweets=" + tweets +
                ", retryDelaySeconds=" + retryDelaySeconds +
                '}';
    }
}
