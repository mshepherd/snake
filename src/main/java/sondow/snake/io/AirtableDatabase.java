package sondow.snake.io;

import com.sybit.airtable.Airtable;
import com.sybit.airtable.Base;
import com.sybit.airtable.Query;
import com.sybit.airtable.Table;
import com.sybit.airtable.exception.AirtableException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import sondow.snake.conf.GameConfig;

public class AirtableDatabase implements Database {

    private final GameConfig config;
    private Airtable airtable;
    private Table<AirtableRecord> table;
    private AirtableRecord record;

    AirtableDatabase(Airtable airtable, GameConfig config) {
        this.config = config;
        this.airtable = airtable;
    }

    public AirtableDatabase(GameConfig config) {
        this.config = config;
    }

    public static void main(String[] args) {
        // AirtableDatabase database = new AirtableDatabase(new GameConfigFactory().configure());
        // String state = database.readGameState();
        // System.out.println(state);
        // database.writeGameState("boom");
    }

    @Override public String readGameState() {
        String state;
        AirtableRecord record = getAirtableRecord();
        state = record.getState();
        if (state != null && state.length() <= 0) {
            state = null;
        }
        return state;
    }

    private AirtableRecord getAirtableRecord() {
        if (record == null) {
            Table<AirtableRecord> table = getTable();
            String screenName = config.getTwitterConfig().getUser();
            Query query = new AirtableBotStateQuery(screenName);
            List results;
            try {
                results = table.select(query);
                if (results.size() <= 0) {
                    createRecord(screenName);
                    results = table.select(query);
                }
            } catch (AirtableException e) {
                throw new RuntimeException(e);
            }

            assert results.size() == 1;
            record = (AirtableRecord) results.get(0);
        }
        return record;
    }

    private void createRecord(String screenName) {
        Table<AirtableRecord> table = getTable();
        AirtableRecord record = new AirtableRecord();
        record.setName(screenName);
        try {
            table.create(record);
        } catch (AirtableException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private Table<AirtableRecord> getTable() {
        if (table == null) {
            Base base;
            try {
                if (airtable == null) {
                    airtable = new Airtable().configure(config.getAirtableApiKey());
                }
                base = airtable.base(config.getAirtableBaseId());
            } catch (AirtableException e) {
                throw new RuntimeException(e);
            }
            table = base.table("SNAKE", AirtableRecord.class);
        }
        return table;
    }

    @Override public void writeGameState(String state) {
        Table<AirtableRecord> table = getTable();
        AirtableRecord record = getAirtableRecord();
        record.setState(state);
        try {
            table.update(record);
        } catch (AirtableException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public void deleteGameState() {
        writeGameState("");
    }
}
