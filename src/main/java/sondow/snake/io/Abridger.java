package sondow.snake.io;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import sondow.snake.game.GameSummary;

/**
 * This class chooses which game states to tweet.
 */
public class Abridger {

    public List<GameSummary> abridge(List<GameSummary> gameSummaries) {

        int softMax = 5;

        List<GameSummary> abridged;
        if (gameSummaries.size() <= softMax) {
            abridged = gameSummaries;
        } else {
            abridged = new ArrayList<>();
            // If there are a lot of game summaries where a target gets eaten, then just include
            // all the game states right before each target gets eaten, as well as the first and
            // last game state, even if there are more than 5 game states to display this way.
            TreeSet<Integer> indicesOfStepsToTweet = new TreeSet<>();
            TreeSet<Integer> indicesOfStepsRightBeforeEating = new TreeSet<>();
            for (int i = 0; i < gameSummaries.size(); i++) {
                // This is one where a target gets eaten. We should show the one right before
                // this one so the clock will show how many points we get for eating this
                // target.
                if (i >= 1 && gameSummaries.get(i).getAtomicMovesSinceEating() == 0) {
                    indicesOfStepsRightBeforeEating.add(i - 1);
                }
            }
            // Include the pre-eating game states, first and last states, and enough gap-fillers
            // to total 5 tweets
            if (gameSummaries.size() >= 1) {
                indicesOfStepsToTweet.add(0);
                indicesOfStepsToTweet.add(gameSummaries.size() - 1);
            }
            indicesOfStepsToTweet.addAll(indicesOfStepsRightBeforeEating);

            while (indicesOfStepsToTweet.size() < softMax) {
                addMiddleNumberToWidestGap(indicesOfStepsToTweet);
            }
            for (Integer index : indicesOfStepsToTweet) {
                abridged.add(gameSummaries.get(index));
            }
        }

        return abridged;
    }

    void addMiddleNumberToWidestGap(TreeSet<Integer> integerSet) {
        // Find and collect all the gap ranges
        List<List<Integer>> gaps = new ArrayList<>();
        List<Integer> integerList = new ArrayList<>(integerSet);
        if (integerList.size() >= 2) {
            for (int i = 1; i < integerList.size(); i++) {
                Integer prev = integerList.get(i - 1);
                Integer next = integerList.get(i);
                int diff = next - prev;
                if (diff >= 2) {
                    List<Integer> gap = new ArrayList<>();
                    for (int k = prev+1; k < next; k++) {
                        gap.add(k);
                    }
                    gaps.add(gap);
                }
            }
        }

        // Find the longest gap. If it's a tie, pick the gap closest to the end.
        if (gaps.size() >= 1) {
            int greatestGapWidth = 0;
            int indexOfWidestGap = 0;
            for (int i = 0; i < gaps.size(); i++) {
                int size = gaps.get(i).size();
                if (size >= greatestGapWidth) {
                    greatestGapWidth = size;
                    indexOfWidestGap = i;
                }
            }
            List<Integer> widestGap = gaps.get(indexOfWidestGap);
            // Pick the middle number of widestGap.
            Integer median = widestGap.get(widestGap.size() / 2);
            integerSet.add(median);
        }
    }
}
