package sondow.snake.io;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.DeleteItemRequest;
import com.amazonaws.services.dynamodbv2.model.DescribeTableResult;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import sondow.snake.conf.GameConfig;
import sondow.snake.conf.GameConfigFactory;

public class AwsDynamoDatabase {

    private static final Integer PRIMARY_KEY_VALUE = 1;
    private static final String PRIMARY_KEY_ATTR_NAME = "PRIMARY_KEY";
    private static final String CURRENT_ATTR_NAME = "CURRENT";
    private final String tableSuffix;
    private final AWSCredentialsProvider cred;

    private AmazonDynamoDB db;

    public AwsDynamoDatabase(AWSCredentialsProvider awsCredentialsProvider, String tableSuffix) {
        this.cred = awsCredentialsProvider;
        this.tableSuffix = tableSuffix.toUpperCase();
    }

    public static void main(String[] args) {
        GameConfig gameConfig = new GameConfigFactory().configure();
        new AwsDynamoDatabase(gameConfig, "MANUALTEST").readGameState();
    }

    private String buildTableName() {
        return "SNAKE_" + tableSuffix;
    }

    /**
     * @return the json string representing the game state, or null if nothing in the database yet
     */
    public String readGameState() {
        String tableName = buildTableName();
        AttributeValue primKeyAttr = new AttributeValue();
        primKeyAttr.setN("" + PRIMARY_KEY_VALUE);
        Map<String, AttributeValue> key = new HashMap<>();
        key.put(PRIMARY_KEY_ATTR_NAME, primKeyAttr);
        GetItemRequest getItemRequest = new GetItemRequest(tableName, key);
        GetItemResult itemResult;
        try {
            itemResult = getItem(getItemRequest);
        } catch (ResourceNotFoundException rnfe) {
            createTable(tableName);
            itemResult = getItem(getItemRequest);
        }
        Map<String, AttributeValue> item = itemResult.getItem();
        String result;
        if (item == null) {
            result = null;
        } else {
            result = item.get(CURRENT_ATTR_NAME).getS();
        }
        return result;
    }

    public void writeGameState(String json) {
        String tableName = buildTableName();
        Map<String, AttributeValue> itemMap = makePrimaryKeyAttrMap();
        itemMap.put(CURRENT_ATTR_NAME, new AttributeValue(json));
        PutItemRequest request = new PutItemRequest(tableName, itemMap);
        try {
            putItem(request);
        } catch (ResourceNotFoundException rnfe) {
            createTable(tableName);
            putItem(request);
        }
    }

    public void deleteGameState() {
        String tableName = buildTableName();
        DeleteItemRequest request = new DeleteItemRequest(tableName, makePrimaryKeyAttrMap());
        getDb().deleteItem(request);
    }

    private Map<String, AttributeValue> makePrimaryKeyAttrMap() {
        Map<String, AttributeValue> itemMap = new HashMap<>();
        AttributeValue primKeyAttrValue = new AttributeValue();
        primKeyAttrValue.setN("" + PRIMARY_KEY_VALUE);
        itemMap.put(PRIMARY_KEY_ATTR_NAME, primKeyAttrValue);
        return itemMap;
    }

    private PutItemResult putItem(PutItemRequest request) {
        return getDb().putItem(request);
    }

    private GetItemResult getItem(GetItemRequest getItemRequest) {
        return getDb().getItem(getItemRequest);
    }

    private DescribeTableResult createTable(String tableName) {
        // Make the table cuz it ain't there yet.
        KeySchemaElement keySchemaElement = new KeySchemaElement(PRIMARY_KEY_ATTR_NAME, "HASH");
        List<KeySchemaElement> schema = Collections.singletonList(keySchemaElement);
        CreateTableRequest createTableRequest = new CreateTableRequest(tableName, schema);

        AttributeDefinition attr = new AttributeDefinition(PRIMARY_KEY_ATTR_NAME, "N");
        List<AttributeDefinition> attrs = Collections.singletonList(attr);
        createTableRequest.setAttributeDefinitions(attrs);

        ProvisionedThroughput throughput = new ProvisionedThroughput(10L, 10L);
        createTableRequest.setProvisionedThroughput(throughput);
        AmazonDynamoDB db = getDb();
        db.createTable(createTableRequest);
        return waitForActiveTable(tableName, db);
    }

    private DescribeTableResult waitForActiveTable(String tableName, AmazonDynamoDB db) {
        DescribeTableResult describeTableResult = db.describeTable(tableName);
        String tableStatus = describeTableResult.getTable().getTableStatus();
        int timesItWasActive = 0;
        // Even after the table is marked ACTIVE, it still takes a few more seconds before you can
        // put data into it. This is probably an eventual consistency bug with no external perfect
        // workaround, so we'll just wait and hope it works after 4 seconds. 🤷‍♀️
        while (timesItWasActive < 4) {
            System.out.print(tableStatus + " ");
            waitASec();
            tableStatus = describeTableResult.getTable().getTableStatus();
            if ("ACTIVE".equals(tableStatus)) {
                timesItWasActive++;
            }
            describeTableResult = db.describeTable(tableName);
        }
        System.out.println();
        return describeTableResult;
    }

    private void waitASec() {
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    private AmazonDynamoDB getDb() {
        if (db == null) {
            String r = System.getenv("AWS_DEFAULT_REGION");
            if (r == null) {
                r = "us-west-2";
            }
            db = AmazonDynamoDBClientBuilder.standard().withRegion(r).withCredentials(cred).build();
        }
        return db;
    }
}
