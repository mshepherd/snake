package sondow.snake.io;

import java.util.Date;

/**
 * An object-relational-mapping (ORM) Java bean to match a record in the Airtable database.
 */
public class AirtableRecord {
    private String id;
    private String name;
    private String state;
    private Date createdTime;

    /**
     * @return the unique, immutable identifier Airtable gives to the record
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name of the twitter handle where the game is running
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the compressed, text-encoded game state
     */
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return when the record was created in the database (supplied by Airtable)
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    @Override public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        AirtableRecord record = (AirtableRecord) o;

        if (getId() != null ? !getId().equals(record.getId()) : record.getId() != null) {
            return false;
        }
        if (getState() != null ? !getState().equals(record.getState()) :
                record.getState() != null) {
            return false;
        }
        if (getName() != null ? !getName().equals(record.getName()) : record.getName() != null) {
            return false;
        }
        return getCreatedTime() != null ? getCreatedTime().equals(record.getCreatedTime()) :
                record.getCreatedTime() == null;
    }

    @Override public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getState() != null ? getState().hashCode() : 0);
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getCreatedTime() != null ? getCreatedTime().hashCode() : 0);
        return result;
    }

    @Override public String toString() {
        return "AirtableRecord{" +
                "id='" + id + '\'' +
                ", state='" + state + '\'' +
                ", name='" + name + '\'' +
                ", createdTime=" + createdTime +
                '}';
    }
}
