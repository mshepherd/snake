package sondow.snake.io;

import java.io.File;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import sondow.snake.conf.GameConfig;
import sondow.snake.conf.GameConfigFactory;
import twitter4j.Card;
import twitter4j.CardLight;
import twitter4j.CardsTwitterImpl;
import twitter4j.Choice;
import twitter4j.HttpResponse;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.StatusUpdateWithCard;
import twitter4j.StatusWithCard;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.auth.Authorization;
import twitter4j.auth.AuthorizationFactory;
import twitter4j.conf.Configuration;

/**
 * See JavaScript example at https://gist.github.com/fourtonfish/816c5272c3480c7d0e102b393f60bd49.
 * <p>
 * See Python example at https://gist.github.com/fourtonfish/5ac885e5e13e6ca33dca9f8c2ef1c46e
 * <p>
 * More reference to understand the examples above.
 * <p>
 * Endpoints: https://github.com/ttezel/twit/blob/master/lib/endpoints.js
 * <p>
 * Endpoints are also available in twitter4j at twitter4j.conf.ConfigurationBase but
 * caps.twitter.com is missing
 * because it isn't a publicly documented part of Twitter's API.
 * <p>
 * JavaScript promise library Q: https://github.com/kriskowal/q
 *
 * @author @JoeSondow
 */
public class TwitterPollMaker {

    private CardsTwitterImpl twitter;
    private Configuration configuration;

    TwitterPollMaker(Configuration twitterConfig, CardsTwitterImpl twitter) {
        this.twitter = twitter;
        this.configuration = twitterConfig;
    }

    public TwitterPollMaker(Configuration twitterConfig) {
        this.configuration = twitterConfig;
        Authorization auth = AuthorizationFactory.getInstance(twitterConfig);
        twitter = new CardsTwitterImpl(twitterConfig, auth);
    }

    public static void main(String[] args) {
        GameConfig config = new GameConfigFactory().configure();
        Configuration twitConf = config.getTwitterConfig();
        TwitterPollMaker pollMaker = new TwitterPollMaker(twitConf);
        long tweetId = 1078249792721936384L;
        StatusWithCard tweet = pollMaker.readPreviousTweet(tweetId);
        Card card = tweet.getCard();
        Choice[] choices = card.getChoices();
        for (Choice c : choices) {
            System.out.printf("%3d %s\n", c.getCount(), c.getLabel());
        }
    }

    public Status tweet(String text, Long inReplyToStatusId, File file) {
        StatusUpdate update = new StatusUpdate(text);
        update.setMedia(file);
        if (inReplyToStatusId != null && inReplyToStatusId > 0) {
            update.setInReplyToStatusId(inReplyToStatusId);
        }
        try {
            return twitter.updateStatus(update);
        } catch (TwitterException e) {
            throw new RuntimeException(e);
        }
    }

    public Status postPoll(int durationMinutes, String statusText, List<String> choices,
                           Long inReplyToStatusId) {

        int choicesCount = choices.size();
        Map<String, String> params = new LinkedHashMap<>();
        params.put("twitter:api:api:endpoint", "1");
        params.put("twitter:card", "poll" + choices.size() + "choice_text_only");
        params.put("twitter:long:duration_minutes", Integer.toString(durationMinutes));
        ensureTwoToFourChoices(choicesCount);

        for (int i = 0; i < choices.size(); i++) {
            String entry = choices.get(i);
            params.put("twitter:string:choice" + (i + 1) + "_label", entry);
        }
        try {
            CardLight card = twitter.createCard(params);
            StatusUpdateWithCard update = new StatusUpdateWithCard(statusText, card.getCardUri());
            if (inReplyToStatusId != null && inReplyToStatusId > 0) {
                update.setInReplyToStatusId(inReplyToStatusId);
            }
            return twitter.updateStatus(update);
        } catch (TwitterException te) {
            System.out.print("Error... ");
            System.out.print("TwitterException access level: " + te.getAccessLevel() +
                    ", isCausedByNetworkIssue: " + te.isCausedByNetworkIssue() +
                    ", cause: " + te.getCause());
            try {
                Field field = TwitterException.class.getDeclaredField("response");
                field.setAccessible(true);
                HttpResponse response = (HttpResponse) field.get(te);
                new Debug().logHttpResponse(response);
            } catch (NoSuchFieldException | IllegalAccessException reflectionException) {
                reflectionException.printStackTrace();
            }
            throw new RuntimeException(te);
        }
    }

    private void ensureTwoToFourChoices(int choicesCount) {
        if (choicesCount < 2) {
            throw new RuntimeException("Must have at least two poll choices");
        }
        if (choicesCount > 4) {
            throw new RuntimeException("Too many poll choices (max 4)");
        }
    }

    public StatusWithCard readPreviousTweet(Long tweetId) {
        StatusWithCard tweet;
        if (tweetId == null) {
            tweet = null;
        } else {
            try {
                tweet = twitter.showStatusWithCard(tweetId);
            } catch (TwitterException e) {
                throw new RuntimeException(e);
            }
        }
        return tweet;
    }

    /**
     * If the account's twitter bio, or description, ends with " High Score: " and some digits
     * then this method parses the old high score, and if the specified new score is greater than
     * the old high score, this method updates the twitter bio with the new high score.
     *
     * This has caused failures when run from AWS Lambda servers. Something about invalid data from
     * Twitter's API. I haven't been able to reproduce the error locally, so it's hard to
     * troubleshoot quickly, and not that important, so I've turned off usage of this method for
     * now.
     *
     * @param score the score of the current game
     */
    public void updateBioForScore(int score) {
        User user;
        try {
            user = twitter.showUser(configuration.getUser());
        } catch (TwitterException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        String description = user.getDescription();
        Pattern pattern = Pattern.compile("(.*) High Score: ([0-9]+)");
        Matcher matcher = pattern.matcher(description);
        if (matcher.matches()) {
            int oldHighScore = Integer.parseInt(matcher.group(2));
            if (score > oldHighScore) {
                String newDescription = matcher.group(1) + " High Score: " + score;
                String url = user.getURL();
                String location = user.getLocation();
                try {
                    twitter.updateProfile(user.getScreenName(), url, location, newDescription);
                    System.out.println("Updated twitter bio with score " + score + " so now the " +
                            "bio is '" + newDescription + "'");
                } catch (TwitterException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
