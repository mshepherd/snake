package sondow.snake.io;

import com.sybit.airtable.Query;
import com.sybit.airtable.Sort;
import java.util.List;

public class AirtableBotStateQuery implements Query {

    private String twitterScreenName;

    AirtableBotStateQuery(String twitterScreenName) {
        this.twitterScreenName = twitterScreenName;
    }

    @Override public String[] getFields() {
        return new String[] {"state"};
    }

    @Override public Integer getPageSize() {
        return null;
    }

    @Override public Integer getMaxRecords() {
        return null;
    }

    @Override public String getView() {
        return null;
    }

    @Override public List<Sort> getSort() {
        return null;
    }

    @Override public String filterByFormula() {
        return "name='" + twitterScreenName + "'";
    }

    @Override public String getOffset() {
        return null;
    }

    @Override public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        AirtableBotStateQuery that = (AirtableBotStateQuery) o;

        return twitterScreenName != null ? twitterScreenName.equals(that.twitterScreenName) :
                that.twitterScreenName == null;
    }

    @Override public int hashCode() {
        return twitterScreenName != null ? twitterScreenName.hashCode() : 0;
    }
}
