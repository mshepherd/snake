package sondow.snake.io;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import sondow.snake.game.Game;
import sondow.snake.game.Point;
import sondow.snake.game.Snake;
import sondow.snake.game.Target;
import twitter4j.JSONArray;
import twitter4j.JSONException;
import twitter4j.JSONObject;

/**
 * Converts stuff into other stuff, mainly text to snake game and back.
 *
 * @author @JoeSondow
 */
public class Converter {

    private Random random;

    public Converter(Random random) {
        this.random = random;
    }

    public Game makeGameFromJson(String json) {
        try {
            return parse(json);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private Game parse(String json) throws JSONException {
        JSONObject obj = new JSONObject(json);
        String themeName = obj.getString("thm");
        EmojiSet emojiSet = EmojiSet.valueOf(themeName);
        Game game = new Game(emojiSet, random);
        int score = obj.getInt("sc");
        long tweetId = obj.getLong("id");
        int threadLength = obj.getInt("thr");
        JSONArray targetCoordinates = obj.getJSONArray("tg");
        int targetRow = targetCoordinates.getInt(0);
        int targetRCol = targetCoordinates.getInt(1);
        Target target = new Target(targetRow, targetRCol);
        JSONArray snakeData = obj.getJSONArray("sn");
        int headRow = snakeData.getInt(0);
        int headCol = snakeData.getInt(1);
        Snake snake = new Snake();
        snake.addKeyPoint(headRow, headCol);
        String snakeTraversal = snakeData.getString(2);
        char[] snakeTraversalChars = snakeTraversal.toCharArray();
        int atomicMovesSinceEating = 0;
        if (snakeData.length() >= 4) {
            atomicMovesSinceEating = snakeData.getInt(3);
        }

        int lastRow = headRow;
        int lastCol = headCol;

        // Divide up the snake traversal characters into separate letter-number segments.
        // This makes indexing easier to understand.
        List<String> traversalSegments = new ArrayList<>();
        StringBuilder segmentBuilder = null; // Must start null so empty segment not added to list
        for (int i = 0; i < snakeTraversalChars.length; i++) {
            char ch = snakeTraversalChars[i];
            if (Character.isUpperCase(ch)) {
                if (segmentBuilder != null) {
                    traversalSegments.add(segmentBuilder.toString());
                }
                segmentBuilder = new StringBuilder();
            }
            segmentBuilder.append(ch);
        }
        // Add the last segment to the list.
        traversalSegments.add(segmentBuilder.toString());

        for (String traversalSegment : traversalSegments) {
            char direction = traversalSegment.charAt(0);
            String digits = traversalSegment.substring(1);
            int length = Integer.parseInt(digits);
            // Handle U D L R
            if (direction == 'U') {
                lastRow -= length;
            } else if (direction == 'D') {
                lastRow += length;
            } else if (direction == 'L') {
                lastCol -= length;
            } else if (direction == 'R') {
                lastCol += length;
            }
            snake.addKeyPoint(lastRow, lastCol);
        }
        snake.setAtomicMovesSinceEating(atomicMovesSinceEating);

        game.setSnake(snake).setScore(score).setTarget(target).setTweetId(tweetId);

        game.setThreadLength(threadLength);
        return game;
    }

    public String makeJsonFromGame(Game game) {

        Target target = game.getTarget();
        int[] targetDataArray = new int[2];
        targetDataArray[0] = target.getRow();
        targetDataArray[1] = target.getCol();
        Snake snake = game.getSnake();
        Object[] snakeDataArray = buildEncodedSnakeObjectArray(snake);
        Map<String, Object> gameMap = new HashMap<>();
        gameMap.put("sn", snakeDataArray);
        gameMap.put("tg", targetDataArray);
        gameMap.put("thm", game.getEmojiSet().name());
        gameMap.put("sc", game.getScore());
        gameMap.put("thr", game.getThreadLength());
        gameMap.put("id", game.getTweetId());

        return new JSONObject(gameMap).toString();
    }

    private Object[] buildEncodedSnakeObjectArray(Snake snake) {
        Object[] snakeDataArray = new Object[4];
        List<Point> keyPoints = snake.getKeyPoints();
        Point head = keyPoints.get(0);
        int row = head.getRow();
        int col = head.getCol();
        snakeDataArray[0] = row;
        snakeDataArray[1] = col;
        StringBuilder traversalStringBuilder = new StringBuilder();
        for (int i = 1; i < keyPoints.size(); i++) {
            Point point = keyPoints.get(i);
            int curRow = point.getRow();
            int curCol = point.getCol();
            char direction;
            int length;
            if (curRow < row) {
                direction = 'U';
                length = row - curRow;
            } else if (curRow > row) {
                direction = 'D';
                length = curRow - row;
            } else if (curCol < col) {
                direction = 'L';
                length = col - curCol;
            } else if (curCol > col) {
                direction = 'R';
                length = curCol - col;
            } else {
                throw new RuntimeException("Invalid point list " + keyPoints);
            }
            traversalStringBuilder.append(direction).append(length);
            row = curRow;
            col = curCol;
        }
        snakeDataArray[2] = traversalStringBuilder.toString();
        snakeDataArray[3] = snake.getAtomicMovesSinceEating();
        return snakeDataArray;
    }
}
