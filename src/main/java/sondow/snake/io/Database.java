package sondow.snake.io;

public interface Database {

    String readGameState();

    void writeGameState(String state);

    void deleteGameState();
}
